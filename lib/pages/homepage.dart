import 'package:crud_shopping_mall/models/products.dart';
import 'package:crud_shopping_mall/pages/about.dart';
import 'package:crud_shopping_mall/pages/detail_products.dart';
import 'package:crud_shopping_mall/pages/edit_products.dart';
import 'package:crud_shopping_mall/pages/insert_products.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;


class Home extends StatefulWidget {
  const Home({Key? key, required String title}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return productsFromJson(response.body);
    } else {
      throw Exception('Failed to load Products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            ImageSlideshow(
              /// Width of the [ImageSlideshow].
              width: double.infinity,

              /// Height of the [ImageSlideshow].
              height: 170,

              /// The page to show when first creating the [ImageSlideshow].
              initialPage: 0,

              /// The color to paint the indicator.
              indicatorColor: Colors.blue,

              /// The color to paint behind th indicator.
              indicatorBackgroundColor: Colors.grey,

              /// The widgets to display in the [ImageSlideshow].
              /// Add the sample image file into the images folder
              children: [
                Image.asset(
                  'images/dw1.png',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'images/dw2.png',
                  fit: BoxFit.cover,
                ),
              ],

              /// Called whenever the page in the center of the viewport changes.
              onPageChanged: (value) {
                print('Page changed: $value');
              },

              /// Auto scroll interval.
              /// Do not auto scroll with null or 0.
              autoPlayInterval: 3000,

              /// Loops back to first slide.
              isLoop: true,
            ),
            Padding(
              padding: const EdgeInsets.only(),
              child: ListTile(
                leading: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(FontAwesomeIcons.house,color: Colors.black54,),
                ),
                tileColor: Color(0xFFF5ED7D),
                title: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Home',
                    style: TextStyle(
                        fontFamily: "IBM",
                        fontSize: 16,
                        color: Color(0xFF000B2F)),
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Icon(FontAwesomeIcons.circlePlus),
                title: Text('Add Product',
                    style: TextStyle(fontFamily: "IBM", fontSize: 16)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return InsertProducts();
                  }));
                },
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Icon(FontAwesomeIcons.circleInfo),
                title: Text('About',
                    style: TextStyle(fontFamily: "IBM", fontSize: 16)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return About();
                  }));
                },
              ),
            ),
          ],
        ),
      ),

      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: const Color(0xFF1E1E1E),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Builder(builder: (context) {
                  return IconButton(
                      icon: const Icon(
                        Icons.menu,
                        size: 33.0,
                        color: Colors.white,
                      ),
                      tooltip: "Menu",
                      onPressed: () {
                        Scaffold.of(context).openDrawer();
                      });
                }),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Image.asset('images/logosm.png', height: 36),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Center(
            child: Padding(
              padding:
              const EdgeInsets.only(top: 90.0, left: 20.0, right: 20.0),
              child: Column(
                children: [
                  ImageSlideshow(
                    /// Width of the [ImageSlideshow].
                    width: double.infinity,

                    /// Height of the [ImageSlideshow].
                    height: 220,

                    /// The page to show when first creating the [ImageSlideshow].
                    initialPage: 0,

                    /// The color to paint the indicator.
                    indicatorColor: Colors.blue,

                    /// The color to paint behind th indicator.
                    indicatorBackgroundColor: Colors.grey,

                    /// The widgets to display in the [ImageSlideshow].
                    /// Add the sample image file into the images folder
                    children: [
                      Image.asset(
                        'images/pr1.png',
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'images/pr2.png',
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'images/pr3.png',
                        fit: BoxFit.cover,
                      ),
                    ],

                    /// Called whenever the page in the center of the viewport changes.
                    onPageChanged: (value) {
                      print('Page changed: $value');
                    },

                    /// Auto scroll interval.
                    /// Do not auto scroll with null or 0.
                    autoPlayInterval: 3000,

                    /// Loops back to first slide.
                    isLoop: true,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 7.0, left: 18.0, right: 10.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "ALL PRODUCTS",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "IBM",
                                fontWeight: FontWeight.w700,
                                fontSize: 18.0,
                                height: 2),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => InsertProducts()));
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: const [
                                Text(
                                  "Add",
                                  style: TextStyle(
                                      color: Color(0xFFF1C950),
                                      fontSize: 18.0,
                                      fontFamily: "IBM",
                                      fontWeight: FontWeight.normal,
                                      height: 2),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Icon(
                                  Icons.add_circle,
                                  color: Color(0xFFF1C950),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(top: 16),
                  //   child: Text('ALL PRODUCTS 🛍',style: TextStyle(fontSize: 20,fontFamily: 'IBM',color: Colors.white),),
                  // ),
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 270),
            child: Container(
              child: Center(
                child: FutureBuilder<List<Products>>(
                  future: products,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    // By default, show a loading spinner.
                    if (!snapshot.hasData) return CircularProgressIndicator();
                    // Render student lists
                    return Padding(
                      padding: const EdgeInsets.only(top: 100),
                      child: GridView.builder(
                          gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 2 / 3.3,
                          mainAxisSpacing: 1,
                      ),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                            var data = snapshot.data[index];
                            return Center(
                              child: GestureDetector(
                                child: Card(
                                  color: const Color(0xFF2C2C2C),
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(100.0),
                                          topRight: Radius.circular(100.0),
                                          bottomLeft: Radius.circular(50.0),
                                          bottomRight: Radius.circular(50.0))),
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 35.0),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 20.0, left: 15.0),
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Padding(
                                              padding: const EdgeInsets.only(top: 1,left: 37,bottom: 20),
                                              child: Icon(
                                                  FontAwesomeIcons.basketShopping,size: 70,color: Colors.yellow,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 16.0, left: 15.0),
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              data.name,
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 28,
                                                  fontFamily: "IBM",
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 1.0, left: 15.0),
                                          child: Row(
                                            children: [
                                              Text('฿',style: TextStyle(color: Color(0xFFADFF9C),fontSize: 22,fontFamily: 'Inter'),),
                                              Text(
                                                data.price,
                                                style: const TextStyle(
                                                    color: Color(0xFFADFF9C),
                                                    fontSize: 22,
                                                    fontFamily: "Inter",
                                                    fontWeight: FontWeight.w700),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Details(product: data)),
                                  );
                                },
                              ),
                            );
                            },
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}


