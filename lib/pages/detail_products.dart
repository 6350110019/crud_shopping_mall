import 'package:crud_shopping_mall/models/products.dart';
import 'package:crud_shopping_mall/pages/edit_products.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:http/http.dart' as http;

class Details extends StatefulWidget {
  final Products product;

  Details({required this.product});

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void deleteProducts(context) async {
    await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        'pid': widget.product.pid.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text('Are you sure you want to delete this?'),
          actions: <Widget>[
            ElevatedButton.icon(
              label: Text('No'),
              icon: Icon(Icons.cancel),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
                onPrimary: Colors.white,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton.icon(
              label: Text('Yes'),
              icon: Icon(Icons.check_circle),
              style: ElevatedButton.styleFrom(
                primary: Colors.green,
                onPrimary: Colors.white,
              ),
              onPressed: () => deleteProducts(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: const Color(0xFF1E1E1E),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 33.0,left: 20.0,right: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: const Icon(Icons.arrow_back_outlined,size: 30.0,color: Colors.white,),
                  tooltip: "Back",
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
                Row(
                  children: [
                    IconButton(
                      icon:const FaIcon(FontAwesomeIcons.trash,size:23.0,color: Color(
                          0xFFB6B4B4)),
                      tooltip: "Delete",
                      onPressed: () => confirmDelete(context),
                    ),
                  ],
                )
              ],
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 80.0),
              child: Column(
                children:  <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 18.0),
                    child: Text(widget.product.name,
                      style: const TextStyle(
                        fontSize: 40,
                        fontFamily: "IBM",
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 150.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                      child: Icon(
                        FontAwesomeIcons.basketShopping,size: 100,color: Colors.yellow,
                      ),
                  ),
                ],
              ),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 200.0,left: 20.0,right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text('฿',style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold,color: Color(0xFFADFF9C),),),
                      Text(widget.product.price,
                        style: const TextStyle(
                          fontSize: 40,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                          letterSpacing: 1.0,
                          color: Color(0xFFADFF9C),
                        ),
                      ),
                    ],
                  ),
                  IconButton(
                    icon: const Icon(Icons.check_circle,color: Colors.yellowAccent,size: 30.0,),
                    onPressed:() {
                    },
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 300.0,left: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Descriptions:",
                    style: TextStyle(
                        color: Colors.white38,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0,
                        fontSize: 16.0
                    ),
                  ),
                )
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 360.0,left: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children:  <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(widget.product.description,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "IBMSUB",
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0,
                        fontSize: 16.0
                    ),
                  ),
                )
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(bottom: 30.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: 320,
                height: 60,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: const Color(0xFF6CB70A),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      )
                  ),
                  onPressed: ()  => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => EditProducts(product: widget.product),
                    ),
                  ),

                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: const [
                      Text("แก้ไขสินค้านี้",
                        style: TextStyle(
                            color: Color(0xFFE0E0E0),
                            fontSize: 20.0,
                            fontFamily: "IBM",
                            fontWeight: FontWeight.w700,
                            height: 2
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon( // <-- Icon
                        Icons.edit,
                        size: 24.0,
                        color: Color(0xFFE0E0E0),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    //   Scaffold(
    //   appBar: AppBar(
    //     title: Text('Product Details'),
    //     actions: <Widget>[
    //       IconButton(
    //         icon: Icon(Icons.delete),
    //         onPressed: () => confirmDelete(context),
    //       ),
    //     ],
    //   ),
    //   body: Padding(
    //     padding: const EdgeInsets.only(top: 100),
    //     child: Container(
    //       padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
    //       height: MediaQuery.of(context).size.height * 0.5,
    //       width: MediaQuery.of(context).size.width * 1,
    //       child: Card(
    //         color: Colors.green[800],
    //         shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(12.0),
    //         ),
    //         elevation: 8,
    //         child: Container(
    //           child: (Column(
    //             crossAxisAlignment: CrossAxisAlignment.center,
    //             children: <Widget>[
    //               SizedBox(height: 120.0),
    //               Text(
    //                 widget.product.name,
    //                 style: TextStyle(
    //                   fontSize: 30.0,
    //                   fontWeight: FontWeight.bold,
    //                   color: Colors.black,
    //                 ),
    //               ),
    //               SizedBox(height: 10.0),
    //               Text(
    //                 'Price: \$${widget.product.price}',
    //                 style: TextStyle(
    //                   fontSize: 18.0,
    //                   color: Colors.black,
    //                 ),
    //               ),
    //               SizedBox(height: 10.0),
    //               Text(
    //                 'Description: ${widget.product.description}',
    //                 style: TextStyle(
    //                   fontSize: 18.0,
    //                   color: Colors.black,
    //                 ),
    //               ),
    //             ],
    //           )),
    //         ),
    //       ),
    //     ),
    //   ),
    //   floatingActionButton: FloatingActionButton(
    //     child: Icon(Icons.edit),
    //     onPressed: () => Navigator.of(context).push(
    //       MaterialPageRoute(
    //         builder: (context) => EditProducts(product: widget.product),
    //       ),
    //     ),
    //   ),
    // );
  }
}
