import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;


class InsertProducts extends StatefulWidget {
  @override
  State<InsertProducts> createState() => _InsertProductsState();
}

class _InsertProductsState extends State<InsertProducts> {
  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request to create new data
  Future _createProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: const Color(0xFF1E1E1E),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 33.0,left: 20.0,right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: const Icon(Icons.arrow_back_outlined,size: 30.0,color: Colors.white,),
                    tooltip: "Back",
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text('Add Product',style: TextStyle(fontSize: 28,fontFamily: 'IBM',color: Colors.yellow),),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 5.0),
              // height: MediaQuery.of(context).size.height * 0.35,
              // width: MediaQuery.of(context).size.width * 1,
              height: 350,
              child: Card(
                margin: EdgeInsets.only(top: 6),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6.0),
                ),
                elevation: 8,
                child: Container(
                  child: (Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 19.0),
                        child: text(
                          nameController: nameController,
                          label: "Product Name",
                          hint: "Enter Product Name",
                        ),
                      ),
                      SizedBox(height: 10.0),
                      text(
                        nameController: priceController,
                        label: "Price",
                        hint: "Enter product price",
                      ),
                      SizedBox(height: 10.0),
                      text(
                        nameController: descriptionController,
                        label: "Description",
                        hint: "Enter product description",
                      ),
                      SizedBox(height: 25.0),
                      ElevatedButton.icon(
                        label: Text('Save'),
                        icon: Icon(Icons.save_outlined),
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          minimumSize: Size(320, 55),
                          primary: Colors.teal,
                          onPrimary: Colors.white,
                        ),
                        onPressed: () {
                          _onConfirm(context);
                        },
                      ),
                    ],
                  )),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}

class text extends StatelessWidget {
  const text({
    Key? key,
    required this.nameController,
    required this.label,
    required this.hint,
  }) : super(key: key);

  final TextEditingController nameController;
  final label;
  final hint;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, right: 25, top: 5),
      child: Container(
          child: TextField(
            controller: nameController,
            decoration: InputDecoration(
              filled: true,
              labelText: label,
              hintText: hint,
              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0)),),
            ),
          )),
    );
  }
}
